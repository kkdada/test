<?php

return [

    'pdo' => [
        'driver' => 'mysql',
        'host' => '192.168.181.142',
        'db_name' => 'tibame',
        'db_username' => 'tibame',
        'db_user_password' => 'tibame',
        'default_fetch' => PDO::FETCH_OBJ,
    ],
    'mysqli' => [
        'host' => '192.168.181.142',
        'db_name' => 'tibame',
        'db_username' => 'tibame',
        'db_user_password' => 'tibame',
        'default_fetch' => MYSQLI_ASSOC,
    ],
];
